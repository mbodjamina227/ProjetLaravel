<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Aminata Mbodj',
            'email' => 'mbodjamina227@gmail.com',
            'password' => bcrypt('passer'),
        ]);
        DB::table('users')->insert([
            'name' => 'Mohamed Amar',
            'email' => 'mohamed@gmail.com',
            'password' => bcrypt('passer'),
        ]);
    }
}
