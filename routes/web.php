<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
/**Lien post pour ajouter un étudiant*/
Route::post('etudiants/add', 'EtudiantController@add')->name('addEtudiant');
/**Lien get pour afficher l'information sur  un étudiant*/
Route::get('etudiants/show/{id}', 'EtudiantController@show')->name('showEtudiant')->where('id','[0-9]+');
/**Lien get pour afficher le formulaire de modification  d'un étudiant*/
Route::get('etudiants/edit/{id}', 'EtudiantController@edit')->name('editEtudiant')->where('id','[0-9]+');
/**Lien post pour afficher le formulaire de modification  d'un étudiant*/
Route::get('etudiants/delete/{id}', 'EtudiantController@delete')->name('deleteEtudiant')->where('id','[0-9]+');
/**Lien modifier valider la modification*/
Route::put('etudiants/update/{id}', 'EtudiantController@update')->name('updateEtudiant')->where('id','[0-9]+');

