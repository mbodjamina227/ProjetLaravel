@if(isset($etudiant))
	{!! Form::model($etudiant,['route' => ['updateEtudiant',$etudiant->id],'method' => 'put'])!!}
@endif
{!!Form::label("{{trans('etudiant.nom')}}" ,trans('etudiant.nom'))!!}
{!!Form::text('nom')!!}
{!!Form::label("{{trans('etudiant.prenom')}}",trans('etudiant.prenom'))!!}
{!!Form::text('prenom')!!}