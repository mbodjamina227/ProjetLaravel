@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">{{trans('etudiant.infoetudiant')}}</div>
				<div class="panel-body" >
						<div><strong>{{trans('etudiant.nom')}}: </strong> {{ $etudiant->nom }}</div>
  						<div><strong>{{trans('etudiant.prenom')}}: </strong> {{ $etudiant->prenom }}</div>	
				</div>
			</div>
		</div>
		
	</div>
	
</div>
@endsection