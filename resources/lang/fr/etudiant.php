<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'msgenregistrementok'   => 'Etudiant enregistré avec succès',
    'nom'   => 'Nom',
    'prenom'   => 'Prénom',
    'titremodification'  => 'Mis à jour d\'un étudiant ',
    'msgmisajourok' => 'Etudiant modifié avec succès',
    'infoetudiant' => 'Détails de l\'étudiant',
    'msgsuppression' => 'Etudiant supprimé avec succès',

];
